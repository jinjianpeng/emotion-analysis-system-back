const express = require('express')
const app = express()
const fs = require('fs');
const iconv = require('iconv-lite');
const _ = require('lodash')
const util = require('util')
const child_process = require('child_process')
const exec = util.promisify(child_process.exec)
const readFile = util.promisify(fs.readFile)

app.use(require('cors')())
app.use(express.json()) // for parsing application/json
// 静态资源管理
app.get('/person', function(req, res){
    let results  = '';
    exec('python test.py').then((result)=>{
        results = result.stdout
        console.log(result.stdout);
        res.send([
            {
                id: '4566',
                name:' 张三',
                age:'12',
                hobby:'play',
                data:results
            }
        ])
    });
})
app.get('/getTable', async function(req, res){
    let file_data = await readFile('./output.txt', {encoding:'binary'});
    let buf = Buffer.from(file_data,'binary');
    var tmpstr = iconv.decode(buf, 'GBK'); //使用iconv转成中文格式
    console.log(tmpstr);
    var repStr = tmpstr.replace(/Timestamp\(\'(\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2})\'\)/g, '$1')
    var resultArr = repStr.split('&');
    var result = resultArr.map((item)=>{
        item = item.substring(1, item.length-1); // 去掉[ ]符号
        return item.split(',') // 把数组里的字符串转换为数组
    })

    // let jsonstr = JSON.parse(str);
    res.send( {
        data: result
    })
})


app.listen(4000, ()=>{
    console.log('app is start');
})